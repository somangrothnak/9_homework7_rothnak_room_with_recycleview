package com.somee.rothnaksomang.homework7_room_persistence_recycleview.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.CategoryEntity;

import java.util.List;

@Dao
public interface CategoryDao {
    @Insert
    void insert(CategoryEntity categoryEntity);

    @Insert
    void insert(CategoryEntity... categoryEntities);

    @Query("select * from tbCategory")
    List<CategoryEntity> getCategoryEntities();

}
