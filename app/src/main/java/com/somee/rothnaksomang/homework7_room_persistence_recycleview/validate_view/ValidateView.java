package com.somee.rothnaksomang.homework7_room_persistence_recycleview.validate_view;

import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

public class ValidateView {
    public ValidateView(){}

    public Boolean checkTextView(TextView textView){
        if(textView.getText().toString().equals("") || textView.getText().toString()==null){
            return false;
        }
        return true;
    }
    public Boolean checkEditView(EditText editText){
        if(editText.getText().toString().equals("") || editText.getText().toString()==null){
            return false;
        }
        return true;
    }
    public Boolean checkRadioButton(RadioButton radioButton){
        if(radioButton.getText().toString().equals("") || radioButton.getText().toString()==null){
            return false;
        }
        return true;
    }
    public Boolean checkCheckBok(CheckBox checkBox){
        if(checkBox.getText().toString().equals("") || checkBox.getText().toString()==null){
            return false;
        }
        return true;
    }
    public Boolean checkImageView(ImageView imageView){
        if(imageView.getDrawable()==null){
            return false;
        }
        return true;
    }
}
