package com.somee.rothnaksomang.homework7_room_persistence_recycleview.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.somee.rothnaksomang.homework7_room_persistence_recycleview.R;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.callback.CallBack;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.dao.BookDao;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.dao.CategoryDao;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.database.BookDatabase;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.BookEntity;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.CategoryEntity;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.validate_view.ValidateView;

import java.util.ArrayList;

public class EditDialog extends DialogFragment {
    private static final int GALLARY=1;
    BookEntity bookEntity;
    ArrayList<BookEntity> bookEntities;
    ArrayList<String> categoryEntity;
    ArrayList<CategoryEntity> categoryEntities;
    Uri image;
    ValidateView validateView;
    BookDao bookDao;
    CallBack callBack;
    CategoryDao categoryDao;
    ImageView ivImage;
    int position;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        categoryEntities=new ArrayList<>();
        categoryEntity=new ArrayList<>();
        bookEntities=new ArrayList<>();
        validateView=new ValidateView();
        categoryDao=BookDatabase.getBookDatabase(getActivity()).getCategoryDao();
        bookDao=BookDatabase.getBookDatabase(getActivity()).getBookDao();
        initializeData();
        getData();
        callBack= (CallBack) getActivity().getSupportFragmentManager().findFragmentById(R.id.fl_main_container);
        View view=LayoutInflater.from(getContext()).inflate(R.layout.fragment_update_book,null);
        AlertDialog.Builder builder =new AlertDialog.Builder(getActivity());
        builder.setTitle("Update Book");
        builder.setView(view);
        final ViewHolder viewHolder=new ViewHolder(view);
        ArrayAdapter<String> adapter=new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_dropdown_item ,categoryEntity);
        viewHolder.spCategory.setAdapter(adapter);
        viewHolder.etTitle.setText(bookEntity.getTitle());
        viewHolder.etSize.setText(String.valueOf(bookEntity.getSize()));
        viewHolder.etPrice.setText(String.valueOf(bookEntity.getPrice()));
        //set value to Spinner
        for(int j=0;j<categoryEntities.size();j++){
            if(categoryEntities.get(j).getId()==bookEntity.getCategoryId()){
                viewHolder.spCategory.setSelection(j);
            }
        }
        ivImage.setImageURI(Uri.parse(bookEntity.getImage()));

        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(!validateView.checkEditView(viewHolder.etTitle)){
                    viewHolder.etTitle.requestFocus();
                    Toast.makeText(getContext(),"Title is required",Toast.LENGTH_SHORT).show();
                }else if(!validateView.checkEditView(viewHolder.etSize)){
                    viewHolder.etSize.requestFocus();
                    Toast.makeText(getContext(),"Size is required",Toast.LENGTH_SHORT).show();
                }else if (!validateView.checkEditView(viewHolder.etPrice)){
                    viewHolder.etPrice.requestFocus();
                    Toast.makeText(getContext(),"Price is required",Toast.LENGTH_SHORT).show();
                }else if (!validateView.checkImageView(ivImage)){
                    Toast.makeText(getContext(),"Image is required",Toast.LENGTH_SHORT).show();
                }else{
                    int id = 0;
                    for(int j=0;j<categoryEntities.size();j++){
                        if(viewHolder.spCategory.getSelectedItem().toString()==categoryEntities.get(j).getCategoryName()){
                            id=categoryEntities.get(j).getId();
                        }
                    }
                    BookEntity book =new BookEntity(id,viewHolder.etTitle.getText().toString(),Double.parseDouble(viewHolder.etSize.getText().toString()),Double.parseDouble(viewHolder.etPrice.getText().toString()),image.toString());
                    bookDao.updateBook(book);
                    callBack.bookUpdate(book,position);
                    Toast.makeText(getContext(),"Success",Toast.LENGTH_SHORT).show();
                }
            }
        });

        viewHolder.btnChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent,GALLARY);
            }
        });

        return builder.create();
    }

    class ViewHolder{
        FloatingActionButton btnChooseImage;
        EditText etTitle,etSize,etPrice;
        Spinner spCategory;

        public ViewHolder(View view){
            spCategory=view.findViewById(R.id.spCategory);
            btnChooseImage=view.findViewById(R.id.btn_choose_image);
            etTitle=view.findViewById(R.id.et_title);
            etSize=view.findViewById(R.id.et_size);
            etPrice=view.findViewById(R.id.et_price);
            ivImage=view.findViewById(R.id.iv_image);
        }
    }
    public void initializeData(){
        categoryEntities= (ArrayList<CategoryEntity>) categoryDao.getCategoryEntities();

        for(int j=0;j<categoryEntities.size();j++){
            categoryEntity.add(categoryEntities.get(j).getCategoryName());
        }

    }
    public void getData(){
        bookEntities= (ArrayList<BookEntity>) bookDao.getBooks();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==GALLARY && data!=null){
            image=data.getData();
            ivImage.setImageURI(image);
        }
    }
    public void setData(BookEntity bookEntity,int position){
        this.bookEntity=bookEntity;
        this.position=position;
    }
}
