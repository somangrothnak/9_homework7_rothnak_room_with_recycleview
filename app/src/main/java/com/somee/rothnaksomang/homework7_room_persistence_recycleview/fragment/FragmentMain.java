package com.somee.rothnaksomang.homework7_room_persistence_recycleview.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.somee.rothnaksomang.homework7_room_persistence_recycleview.R;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.adapter.BookAdapter;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.callback.CallBack;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.dao.BookDao;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.dao.CategoryDao;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.database.BookDatabase;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.dialog.EditDialog;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.dialog.InsertDialog;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.BookEntity;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.CategoryEntity;

import java.util.ArrayList;

public class FragmentMain extends Fragment
    implements CallBack
{
    ArrayList<BookEntity> bookEntities;
    ArrayList<CategoryEntity> categoryEntities;
    RecyclerView recyclerView;
    BookAdapter bookAdapter;
    View view;
    AppCompatButton btnDonateBook;
    ArrayList<String> categoryString;
    BookDao bookDao;
    CategoryDao categoryDao;
    static FragmentMain fragment;

    public static FragmentMain newInstance(){
        if(fragment==null){
            fragment=new FragmentMain();
        }
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        categoryEntities=new ArrayList<>();
        bookEntities=new ArrayList<>();
        categoryString=new ArrayList<>();
        bookDao=BookDatabase.getBookDatabase(getContext()).getBookDao();
        categoryDao=BookDatabase.getBookDatabase(getContext()).getCategoryDao();
        insertCategory();
        initializeData();
        getData();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view=inflater.from(getContext()).inflate(R.layout.fragment_main,container,false);
        recyclerView=view.findViewById(R.id.rv_items);
        bookAdapter=new BookAdapter(this,bookEntities,categoryEntities,categoryString);
        btnDonateBook=view.findViewById(R.id.btn_donate_book);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(bookAdapter);
        setDonateBook();
    }
    public void initializeData(){
        categoryEntities= (ArrayList<CategoryEntity>) categoryDao.getCategoryEntities();

        for(int j=0;j<categoryEntities.size();j++){
            categoryString.add(categoryEntities.get(j).getCategoryName());
        }

    }
    public void setDonateBook(){
            btnDonateBook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    InsertDialog insertDialog=new InsertDialog();
                    insertDialog.show(getActivity().getSupportFragmentManager(),"Insert Books");
                }
            });
    }
    public void replaceFragment(Fragment desFragment){
        FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
        FragmentTransaction transaction=fragmentManager.beginTransaction();
        transaction.replace(R.id.fl_main_container,desFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
    public void getData(){
        bookEntities= (ArrayList<BookEntity>) bookDao.getBooks();
    }

    public void insertCategory(){
        if(categoryDao.getCategoryEntities()==null){
            categoryDao.insert(new CategoryEntity(1,"Computer Science"));
            categoryDao.insert(new CategoryEntity(2,"History"));
            categoryDao.insert(new CategoryEntity(3,"Business"));
            Toast.makeText(getContext(),"Insert Category Success",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void removeBook(final BookEntity bookEntity) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        builder.setTitle("Remove");
        builder.setMessage("Do you want to Remove this book?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                bookEntities.remove(bookEntity);
                bookAdapter.notifyDataSetChanged();
                bookDao.deleteBook(bookEntity);

            }
        });
        builder.setNegativeButton("No",null);
        AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }

    @Override
    public void readBook(BookEntity bookEntity, ArrayList<CategoryEntity> categoryEntities) {
        //create object of Fragment
        FragmentReadBook fragmentReadBook=FragmentReadBook.newInstance();
        //set object Book to Layout Read
        fragmentReadBook.setBookEntity(bookEntity);
        //set object Category to Layout Read
        fragmentReadBook.setCategoryEntities(categoryEntities);
        //Replace New Fragment
        replaceFragment(fragmentReadBook);
    }

    @Override
    public void bookEdit(BookEntity bookEntity, int position) {
        EditDialog editDialog=new EditDialog();
        editDialog.setData(bookEntity,position);
        editDialog.show(getActivity().getSupportFragmentManager(),"Update");
    }

    @Override
    public void bookInsert() {
        bookEntities.clear();
        bookEntities.addAll(bookDao.getBooks());
        bookAdapter.notifyDataSetChanged();
    }

    @Override
    public void bookUpdate(BookEntity bookEntity, int position) {
        bookEntities.set(position,bookEntity);
        bookAdapter.notifyItemChanged(position);
    }
}
