package com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "tbBook")
public class BookEntity {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int categoryId;
    private String title;
    private double size;
    private double price;
    private String image;

    public BookEntity(){}

    public BookEntity(int id, int categoryId, String title, double size, double price, String image) {
        this.id = id;
        this.categoryId = categoryId;
        this.title = title;
        this.size = size;
        this.price = price;
        this.image = image;
    }
    public BookEntity(int categoryId, String title, double size, double price, String image) {
        this.categoryId = categoryId;
        this.title = title;
        this.size = size;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "BookEntity{" +
                "id=" + id +
                ", categoryId=" + categoryId +
                ", title='" + title + '\'' +
                ", size=" + size +
                ", price=" + price +
                ", image=" + image +
                '}';
    }
}
