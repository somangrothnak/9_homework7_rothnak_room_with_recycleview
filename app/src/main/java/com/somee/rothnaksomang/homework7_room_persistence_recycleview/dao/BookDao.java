package com.somee.rothnaksomang.homework7_room_persistence_recycleview.dao;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.BookEntity;
import java.util.List;

@Dao
public interface BookDao {
    // insert only one book
    @Insert
    void insert(BookEntity bookEntity);
    // insert multiple books
    @Insert
    void insert(BookEntity... bookEntities);

    @Query("select * from tbbook")
    List<BookEntity> getBooks();

    @Query("select * from tbBook where id>:id")
    BookEntity getBook(int id);

    @Delete()
    void deleteBook(BookEntity bookEntity);

    @Update
    void updateBook(BookEntity bookEntity);

}
