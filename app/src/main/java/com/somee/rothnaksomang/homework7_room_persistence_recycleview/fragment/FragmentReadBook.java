package com.somee.rothnaksomang.homework7_room_persistence_recycleview.fragment;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.somee.rothnaksomang.homework7_room_persistence_recycleview.R;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.BookEntity;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.CategoryEntity;

import java.util.ArrayList;

public class FragmentReadBook extends Fragment {
    BookEntity bookEntity;
    ArrayList<CategoryEntity> categoryEntities;
    FloatingActionButton btnBack;
    TextView tvTitle, tvSize, tvPrice,tvCategory;
    ImageView ivImage;
    static FragmentReadBook fragment;

    public void setBookEntity(BookEntity bookEntity) {
        this.bookEntity = bookEntity;
    }

    public static FragmentReadBook newInstance() {
        if (fragment == null) {
            fragment = new FragmentReadBook();
        }
        return fragment;
    }

    public FragmentReadBook() {
    }

    public void setCategoryEntities(ArrayList<CategoryEntity> categoryEntities) {
        this.categoryEntities = categoryEntities;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.from(getContext()).inflate(R.layout.fragment_read_book, container, false);
        btnBack= view.findViewById(R.id.btn_back);
        tvTitle = view.findViewById(R.id.tv_title);
        tvCategory=view.findViewById(R.id.tv_category);
        tvSize = view.findViewById(R.id.tv_size);
        tvPrice = view.findViewById(R.id.tv_price);
        ivImage = view.findViewById(R.id.iv_image);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //set value to View
        tvTitle.setText(bookEntity.getTitle());
        //set Category Name
        String categoryName="";
        for(int j=0;j<categoryEntities.size();j++){
            if(categoryEntities.get(j).getId()==bookEntity.getCategoryId()){
                categoryName=categoryEntities.get(j).getCategoryName();
            }
        }
        tvCategory.setText(categoryName);

        tvSize.setText(String.valueOf(bookEntity.getSize()));
        tvPrice.setText(String.valueOf(bookEntity.getPrice()));
        ivImage.setImageURI(Uri.parse(bookEntity.getImage()));

        setBtnBack();
    }

    public void setBtnBack() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentMain fragmentMain = FragmentMain.newInstance();
                replaceFragment(fragmentMain);
                Toast.makeText(getContext(), "Success", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void replaceFragment(Fragment desFragment) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.fl_main_container, desFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

}
