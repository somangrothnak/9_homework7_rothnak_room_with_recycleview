package com.somee.rothnaksomang.homework7_room_persistence_recycleview.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.somee.rothnaksomang.homework7_room_persistence_recycleview.R;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.callback.CallBack;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.BookEntity;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.CategoryEntity;

import java.util.ArrayList;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {
    Fragment context;
    ArrayList<BookEntity> bookEntities;
    LayoutInflater  inflater;
    ArrayList<CategoryEntity> categoryEntities;
    CallBack callBack;
    ArrayList<String> categoryString;

    public BookAdapter(Fragment context, ArrayList<BookEntity> bookEntities, ArrayList<CategoryEntity> categoryEntities, ArrayList<String> categoryString){
        this.context=context;
        this.bookEntities=bookEntities;
        this.categoryEntities=categoryEntities;
        inflater=inflater.from(context.getActivity());
        this.categoryString=categoryString;
        callBack= (CallBack) context;
    }

    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=inflater.inflate(R.layout.card_recyclerview,null);
        return new BookViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final BookViewHolder bookViewHolder, final int i) {
        final BookEntity bookEntity=bookEntities.get(i);
        String categoryName="";

        for(int j=0;j<categoryEntities.size();j++){
            if(categoryEntities.get(j).getId()==bookEntity.getCategoryId()){
                categoryName=categoryEntities.get(j).getCategoryName();
            }
        }
        bookViewHolder.tvTitle.setText(bookEntity.getTitle());
        bookViewHolder.tvCategory.setText(categoryName);
        bookViewHolder.tvSize.setText(bookEntity.getSize()+"");
        bookViewHolder.tvPrice.setText(bookEntity.getPrice()+"");
        bookViewHolder.ivBook.setImageURI(Uri.parse(bookEntity.getImage()));
        bookViewHolder.btnOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu=new PopupMenu(context.getActivity(),bookViewHolder.btnOption);
                popupMenu.inflate(R.menu.menu_option);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.item_process_edit:
                                callBack.bookEdit(bookEntity,i);
                                Toast.makeText(context.getActivity(),"Edit",Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.item_process_Remove:
                                Toast.makeText(context.getActivity(),"Remove",Toast.LENGTH_SHORT).show();
                                callBack.removeBook(bookEntity);
                                break;
                            case R.id.item_process_Read:
                                callBack.readBook(bookEntity,categoryEntities);
                                Toast.makeText(context.getActivity(),"Read",Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookEntities.size();
    }

    class BookViewHolder extends RecyclerView.ViewHolder{
        ImageView ivBook;
        TextView tvTitle,tvCategory,tvSize,tvPrice;
        ImageButton btnOption;

        public BookViewHolder(@NonNull View itemView) {
            super(itemView);
            ivBook=itemView.findViewById(R.id.iv_image);
            tvTitle=itemView.findViewById(R.id.tv_title);
            tvCategory=itemView.findViewById(R.id.tv_category);
            tvSize=itemView.findViewById(R.id.tv_size);
            tvPrice=itemView.findViewById(R.id.tv_price);
            btnOption=itemView.findViewById(R.id.ib_option);
        }
    }
}
