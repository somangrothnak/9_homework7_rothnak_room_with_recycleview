package com.somee.rothnaksomang.homework7_room_persistence_recycleview.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.somee.rothnaksomang.homework7_room_persistence_recycleview.dao.BookDao;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.dao.CategoryDao;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.BookEntity;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.CategoryEntity;

@Database(entities = {BookEntity.class,CategoryEntity.class},version = 1)
public abstract class BookDatabase extends RoomDatabase {
    public abstract BookDao getBookDao();
    public abstract CategoryDao getCategoryDao();

    public static BookDatabase getBookDatabase(Context context){
        return Room.databaseBuilder(context,BookDatabase.class,"my-db4")
                .allowMainThreadQueries()
                .build();
    }

}
