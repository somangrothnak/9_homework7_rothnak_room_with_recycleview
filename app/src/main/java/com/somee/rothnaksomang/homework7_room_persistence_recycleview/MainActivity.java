package com.somee.rothnaksomang.homework7_room_persistence_recycleview;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.fragment.FragmentMain;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set Default Fragment
        FragmentMain fragmentMain=FragmentMain.newInstance();
        replaceFragment(fragmentMain);
    }

    public void replaceFragment(Fragment desFragment){
        FragmentManager fragmentManager=this.getSupportFragmentManager();
        FragmentTransaction transaction=fragmentManager.beginTransaction();
        transaction.replace(R.id.fl_main_container,desFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
