package com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "tbCategory")
public class CategoryEntity {
    @PrimaryKey
    private int id;

    private String categoryName;

    public CategoryEntity(){}

    public CategoryEntity(int id, String categoryName) {
        this.id = id;
        this.categoryName = categoryName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
