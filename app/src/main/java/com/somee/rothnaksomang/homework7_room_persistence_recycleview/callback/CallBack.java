package com.somee.rothnaksomang.homework7_room_persistence_recycleview.callback;

import com.somee.rothnaksomang.homework7_room_persistence_recycleview.adapter.BookAdapter;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.BookEntity;
import com.somee.rothnaksomang.homework7_room_persistence_recycleview.entity.CategoryEntity;

import java.util.ArrayList;

public interface CallBack {
    void removeBook(BookEntity bookEntity);
    void readBook(BookEntity bookEntity,ArrayList<CategoryEntity> categoryEntities);
    void bookEdit(BookEntity bookEntity,int position);
    void bookInsert();
    void bookUpdate(BookEntity bookEntity,int position);

}
